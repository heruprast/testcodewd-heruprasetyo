# TestcodeWD_Heru Prasetyo

### Installation

Clone repository:

```sh
$ git clone https://gitlab.com/heruprast/testcodewd-heruprasetyo.git
$ cd testcodewd-heruprasetyo
```

Update composer:

```sh
$ composer update
```

Setting file .env

```sh
$ cp .env.example .env
$ php artisan key:generate
```

Buat database terlebih dahulu kemudian edit file .env
Setelah selesai ikuti langkah salanjutnya:

### Membuat Table

```sh
$ php artisan migrate
$ php artisan db:seed --class=DatabaseSeeder
```

Kita sudah sampai di langkah terakhir penginstallan, ketik kode dibawah untuk menjalankan server.

```sh
$ php artisan serve
```

Buka alamat url dibawah ini

```sh
http://127.0.0.1:8000
```

### Default Login User

##### Admin

email: admin@demo.com
pass : demo123

##### Kasir

email: cashier@demo.com
pass : demo123

##### Pelayan

email: waiter@demo.com
pass : demo123

## Api Documentasi

### Login User

### POST `http://127.0.0.1:8000/api/auth/login`

```sh
http://127.0.0.1:8000/api/auth/login
```

##### Body form data

| Parameter | Keterangan                 |
| --------- | -------------------------- |
| email     | `Isikan email pengguna`    |
| password  | `Isikan password pengguna` |

### GET Daftar Produk

### GET `http://127.0.0.1:8000/api/product`

```sh
http://127.0.0.1:8000/api/product
```

### Update

Bug

-   Tidak bisa malakukan proses pemesanan

Fix Bug

Silahkan marge code anda dengan update terbaru.

```sh
$ git fetch origin master
$ git merge origin/master
```

Selanjutnya ikuti langkah berikut:

```sh
$ php artisan db:seed --class=DatabaseSeeder
```
