@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">Products</h4>
                        <div class="small text-muted">November 2017</div>
                    </div>
                    <div class="col-sm-7 d-none d-md-block">
                        <a class="btn btn-primary float-right" href="{{route('product.create')}}">
                            Add Product
                        </a>
                    </div>
                </div>
                <table class="table table-responsive-sm table-hover mt-4">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                            </th>
                            <th></th>
                            <th>Product</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th class="text-center">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($products as $product)
                        <tr>
                            <td class="text-center" width="10">
                                <input type="checkbox" name="selected[]" value="{{$product->id}}" />
                            </td>
                            <td width="32">
                                <img class="rounded" src="/{{$product->cover}}" width="32" height="32" role="img">
                            </td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->price}}</td>
                            <td class="text-center">
                                @if ($product->status)
                                <span class="badge badge-success"><i class="fa fa-check"></i></span>
                                @else
                                <span class="badge badge-danger"><i class="fa fa-times"></i></span>
                                @endif
                            </td>
                            <td class="text-right">
                                @if (!auth()->user()->hasRole('cashier'))
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('product.edit',$product->id)}}">Edit</a>
                                        <a class="dropdown-item" href="{{route('product.destroy',$product->id)}}"
                                            data-toggle="confirm" data-message="Are you sure?">Hapus</a>
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="table-empty" colspan="6">
                                <div class="table-empty-msg">
                                    <i class="fa fa-warning table-empty-icon"></i> No
                                    records
                                    found
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
