@extends('layouts.master')

@section('content')
<form class="form-horizontal" action="{{route('product.update',$product->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input name="_method" type="hidden" value="PUT">
    <div class="container">
        @include('flash::message')
        <div class="row">
            <div class="col-md-8">
                <div class="animated fadeIn">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group">
                                <label class="col-form-label" for="inputTitle">Title</label>
                                <input name="title" class="form-control{{$errors->has('title')?' is-invalid':''}}"
                                    value="{{old('title',$product->name)}}" id="inputTitle" type="text">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="editor">Image</label>
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="card-title mb-0">Pricing</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputCost">Cost per item</label>
                                <input name="price" id="inputCost" type="number" class="medium" data-prefix="Rp." value="{{old('price',$product->price)}}"
                                    {!!$errors->has('price')?'class="is-invalid"':''!!}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="animated fadeIn">
                    <div class="card bg-light">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="card-title mb-0">Category</strong>
                                </div>
                            </div>
                            <div class="form-group mt-2">
                                @foreach ($categories as $item)
                                <div class="form-check">
                                    <input class="form-check-input" id="radio-{{$item->id}}" type="radio" value="{{$item->id}}"
                                        name="category" {{$item->id == $product->category_id?"checked":""}}>
                                    <label class="form-check-label" for="radio-{{$item->id}}">{{$item->name}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="form-actions float-right">
                    <button class="btn btn-secondary" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Product</button>
                </div>
            </div>
        </div>
    </div>
</form>
@stop
@push('scripts')
@endpush
