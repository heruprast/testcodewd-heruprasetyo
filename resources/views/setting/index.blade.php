@extends('layouts.master')

@section('content')
<div class="container">
    @include('flash::message')
    <div class="animated fadeIn">
        <form class="form-horizontal" method="POST" action="{{route('setting.update')}}" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <strong class="card-title mb-0">Store name and adderss</strong>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="col-form-label" for="title">Store Name</label>
                                        <input name="title" class="form-control" id="title" type="text" value="{{old('title',setting('store_name'))}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="description">Store Address</label>
                                        <textarea name="address" class="form-control" id="description" rows="5"
                                            placeholder="Enter a address for your store">{{old('description',setting('store_address'))}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row mb-4">
                <div class="col-md-12">
                    <div class="form-actions float-right">
                        <button class="btn btn-primary" type="submit">Save setting</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@push('scripts')
<script>
    $(function () {

    });

</script>
@endpush
