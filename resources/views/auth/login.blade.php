<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin | {{setting('site_title')}}</title>
    @include('components.stylesheet')
</head>

<body class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <form method="POST" action="{{ route('auth.login') }}" data-toggle="login">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="cui-envelope-closed"></i>
                                        </span>
                                    </div>
                                    <input class="form-control" type="text" name="email" placeholder="Email">
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="icon-lock"></i>
                                        </span>
                                    </div>
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-primary px-4 btn-block" data-style="slide-down" type="submit"
                                            id="login">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.scripts')
    <script>
        $(function () {
            $('[data-toggle="login"]').on('submit', function (e) {
                var form = $(this);
                var l = Ladda.create(document.querySelector('#login'));

                if (!e.isDefaultPrevented()) {

                    $.ajax({
                        type: 'POST',
                        url: this.action,
                        data: form.serialize(),
                        beforeSend: function (xhr, type) {
                            l.start();
                        },
                        success: function (res) {
                            if (res.status == true) {
                                window.location.href = res.callback;
                            } else {
                                $.growl.error({
                                    message: res.msg
                                });
                            }
                            l.stop();
                        },
                        error: function (e) {
                            l.stop();
                            $.growl.error({
                                message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                            });
                        }
                    });

                    e.preventDefault();
                }
            });
        });

    </script>
</body>

</html>
