<script src="/admins/vendors/jquery/js/jquery.min.js"></script>
<script src="/admins/vendors/popper.js/js/popper.min.js"></script>
<script src="/admins/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="/admins/vendors/pace-progress/js/pace.min.js"></script>
<script src="/admins/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
<script src="/admins/vendors/coreui/coreui/js/coreui.min.js"></script>
<script src="/admins/vendors/chart.js/js/Chart.min.js"></script>
<script src="/admins/vendors/coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js"></script>
<script src="/admins/vendors/input-file/input-file.min.js" type="text/javascript"></script>
<script src="/admins/vendors/alertable/jquery.alertable.min.js" type="text/javascript"></script>
<script src="/admins/vendors/summernote/summernote-bs4.js"></script>
<script src="/admins/vendors/selectize/js/selectize.min.js"></script>
<script src="/admins/js/bootstrap-input-spinner.js"></script>
<script src="/admins/vendors/ladda/js/spin.min.js"></script>
<script src="/admins/vendors/ladda/js/ladda.min.js"></script>
<script src="/admins/vendors/jquery.growl/js/jquery.growl.js"></script>
<script src="/admins/vendors/datatables/datatables.min.js"></script>
<script src="/admins/js/jquery.isotope.min.js"></script>
<script src="/admins/js/jquery.isotope.sloppy-masonry.min.js"></script>
<script src="/admins/js/script.js"></script>
@stack('scripts')
