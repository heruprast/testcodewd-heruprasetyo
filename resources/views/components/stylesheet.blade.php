<link rel="stylesheet" href="/admins/vendors/coreui/icons/css/coreui-icons.min.css">
<link rel="stylesheet" href="/admins/vendors/icofont/icofont.min.css">
<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
<link rel="stylesheet" href="/admins/vendors/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/admins/vendors/simple-line-icons/css/simple-line-icons.css">
<link rel="stylesheet" href="/admins/css/style.css">
<link rel="stylesheet" href="/admins/vendors/pace-progress/css/pace.min.css">
<link rel="stylesheet" href="/admins/vendors/input-file/input-file.css" type="text/css" />
<link rel="stylesheet" href="/admins/vendors/alertable/jquery.alertable.css" type="text/css" />
<link rel="stylesheet" href="/admins/vendors/summernote/summernote-bs4.css">
<link rel="stylesheet" href="/admins/vendors/selectize/css/selectize.bootstrap4.css">
<link rel="stylesheet" href="/admins/vendors/ladda/css/ladda.min.css">
<link rel="stylesheet" href="/admins/vendors/jquery.growl/css/jquery.growl.css">
<link rel="stylesheet" type="text/css" href="/admins/vendors/datatables/datatables.min.css" />
