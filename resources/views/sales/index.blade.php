@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">Sales</h4>
                        <div class="small text-muted">November 2017</div>
                    </div>
                </div>
                <table class="table table-responsive-sm table-hover mt-4">
                    <thead>
                        <tr>
                            <th class="text-center" width="20">#</th>
                            <th>Order Number</th>
                            <th>Table</th>
                            <th>Cashier</th>
                            <th>Grand Total</th>
                            <th>Tanggal Pesanan</th>
                            <th class="text-center">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($orders as $order)
                        <tr>
                            <td class="text-center" width="10">
                                <input type="checkbox" name="selected[]" value="{{$order->id}}" />
                            </td>
                            <td>{{$order->invoice->number}}</td>
                            <td>{{$order->table->name}}</td>
                            <td>{{$order->user->name}}</td>
                            <td>{{$order->total}}</td>
                            <td>{{$order->created_at->format('d M, Y, h:m a')}}</td>
                            <td class="text-center">
                                @if ($order->invoice->status)
                                <span class="badge badge-success">Paid</span>
                                @else
                                <span class="badge badge-danger">Hold</span>
                                @endif
                            </td>
                            <td class="text-right">
                                <div class="btn-group btn-group" role="group" aria-label="Small button group">
                                    <button data-style="slide-down" class="btn btn-primary orderdetail" type="button"
                                        title="View Sale" data-url="{{route('sale.show',$order->id)}}" id="btn-{{$order->id}}">
                                        <i class="icon-list"></i>
                                    </button>

                                    @if ($order->invoice->status)
                                    @if (!$user->hasRole('cashier'))
                                    <button class="btn btn-primary" disabled type="button" title="Payment">
                                        <i class="icon-credit-card"></i>
                                    </button>
                                    @endif
                                    <button class="btn btn-primary" disabled type="button" title="Payment">
                                        <i class="icon-note"></i>
                                    </button>
                                    @else
                                    @if (!$user->hasRole('cashier'))
                                    <button data-style="slide-down" class="btn btn-primary payment" type="button" title="Payment"
                                        data-url="{{route('sale.paymentDetail',$order->id)}}" id="btnPy-{{$order->id}}">
                                        <i class="icon-credit-card"></i>
                                    </button>
                                    @endif
                                    <a href="{{route('pos.edit',$order->id)}}" class="btn btn-primary">
                                        <i class="icon-note"></i>
                                    </a>
                                    @endif

                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="table-empty" colspan="7">
                                <div class="table-empty-msg">
                                    <i class="fa fa-warning table-empty-icon"></i> No
                                    records
                                    found
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div id="wrapper">
                    <div id="receiptData" style="width: auto; max-width: 580px; min-width: 250px; margin: 0 auto;">
                        <div class="not-print"></div>
                        <div id="receipt-data">
                            <div class="print">
                                {{-- <div style="text-align:center;">
                                    <p style="text-align:center;"><strong>SimplePOS</strong><br>Address Line 1<br>Petaling
                                        Jaya<br>012345678</p>
                                    <p></p>
                                </div> --}}
                                <p>
                                    Date: <span id="saleDate">-</span> <br>
                                    Sale No/Ref: <span id="saleRef">-</span><br>
                                    Customer: <span id="customer">-</span> <br>
                                    Cashier: <span id="cashier">-</span> <br>
                                </p>
                                <div style="clear:both;"></div>
                                <table class="table table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 50%; border-bottom: 2px solid #ddd;">Description</th>
                                            <th class="text-center" style="width: 12%; border-bottom: 2px solid #ddd;">Quantity</th>
                                            <th class="text-right" style="width: 24%; border-bottom: 2px solid #ddd;">Price</th>
                                            <th class="text-right" style="width: 26%; border-bottom: 2px solid #ddd;">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody id="itemList"></tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2">Total</th>
                                            <th colspan="2" class="text-right" id="total">0</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Grand Total</th>
                                            <th colspan="2" class="text-right" id="grandTotal">0</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <table class="table table-striped table-condensed" style="margin-top:10px;">
                                    <tbody>
                                        <tr>
                                            <td class="text-right">Paid by :</td>
                                            <td>Cash</td>
                                            <td class="text-right">Amount :</td>
                                            <td id="amount">0</td>
                                            <td class="text-right">Change :</td>
                                            <td id="change">0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                        <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
                            <hr>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <button onclick="window.print();" class="btn btn-block btn-primary">Print</button>
                                </div>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Payment</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-9">
                        <input type="hidden" name="total">
                        <table class="table table-bordered table-sm mb-4">
                            <tbody>
                                <tr>
                                    <td width="25%" style="border-right-color: #FFF !important;">Total Items</td>
                                    <td width="25%" class="text-right"><span id="item_count">0</span></td>
                                    <td width="25%" style="border-right-color: #FFF !important;">Total Payable</td>
                                    <td width="25%" class="text-right"><span id="twt">0</span></td>
                                </tr>
                                <tr>
                                    <td style="border-right-color: #FFF !important;">Total Paying</td>
                                    <td class="text-right"><span id="total_paying">0</span></td>
                                    <td style="border-right-color: #FFF !important;">Balance</td>
                                    <td class="text-right"><span id="balance">0</span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-12">
                                <form method="POST" action="{{route('sale.payment')}}" id="pay">
                                    @csrf
                                    <input type="hidden" name="order_id">
                                    <div class="row">
                                        <div class="form-group col-sm-8">
                                            <label for="amount">Amount</label>
                                            <input name="amount" id="amount" type="number" class="medium" data-prefix="Rp."
                                                value="0" step="500">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="paying">Paying by</label>
                                            <select class="form-control" id="paying">
                                                <option value="1">Cash</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputNote">Payment Note</label>
                                        <textarea class="form-control" id="inputNote"></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="btn-group-lg btn-group-vertical d-flex" role="group" aria-label="First group">
                            <button type="button" class="btn btn-outline-primary cash" value="5000">5000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="10000">10000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="20000">20000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="50000">50000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="100000">100000</button>
                            <button type="button" class="btn btn-warning clear" style="color: white">Clear</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-payment">Payment</button>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script>
    $(function () {

        $('.orderdetail').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var modal = $('#payment')
            l.start()
            $.get(button.data('url'), function (arg) {
                modal.find('#total').text(arg.total)
                modal.find('#grandTotal').text(arg.grandTotal)
                modal.find('#saleDate').text(arg.date)
                modal.find('#saleRef').text(arg.ref)
                modal.find('#cashier').text(arg.cashier)
                modal.find('#amount').text(arg.amount)
                modal.find('#change').text(arg.change)

                var tbody = $('#itemList').html('');
                $.each(arg.items, function (i, item) {
                    tbody.append('<tr>');
                    tbody.append('<td>' + item.name + '</td>');
                    tbody.append('<td class="text-center">' + item.quantity + '</td>');
                    tbody.append('<td class="text-right">' + item.price + '</td>');
                    tbody.append('<td class="text-right">' + item.subtotal + '</td>');
                    tbody.append('</tr>');
                });

            }).done(function () {
                modal.modal('show')
            }).fail(function () {
                $.growl.error({
                    message: "Terjadi kesalahan!"
                });

            }).always(function () {
                l.stop();
            });
        });

        $('.payment').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var modal = $('#paymentModal')
            l.start()
            $.get(button.data('url'), function (arg) {
                var a = modal.find('#amount')
                var t = modal.find('[name="total"]').val(arg.total)
                modal.find('#item_count').text(arg.item)
                modal.find('#twt').text(arg.total)
                modal.find('#total_paying').text(arg.total)
                modal.find('[name="order_id"]').val(arg.order_id)
                a.val(arg.total)

                modal.find('#balance').text(a.val() - t.val())

            }).done(function () {
                modal.modal('show')
            }).fail(function () {
                $.growl.error({
                    message: "Terjadi kesalahan!"
                });

            }).always(function () {
                l.stop();
            });
        });

        $('.cash').on('click', function (e) {
            var amoun = $('#amount');
            var cash = this.value;
            amoun.val(+amoun.val() + +cash)
            var t = $('[name="total"]')
            $('#balance').text(+amoun.val() - +t.val())
        });
        $('.clear').on('click', function (e) {
            var amoun = $('#amount').val(0)
            var t = $('[name="total"]')
            $('#balance').text(0 - t.val())
        });

        $('#btn-payment').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $("#pay");
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize(),
                    beforeSend: function (xhr, type) {
                        l.start();
                    },
                    success: function (res) {
                        if (res.status == true) {
                            $.growl.notice({
                                message: res.msg
                            });
                            location.reload();
                        } else {
                            $.growl.error({
                                message: res.msg
                            });
                        }
                        l.stop();
                    },
                    error: function (e) {
                        $.growl.error({
                            message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                        });
                        l.stop();
                    }
                });
            });
        });
    });

</script>
@endpush
