@extends('layouts.master')

@section('content')
<form action="{{route('category.store')}}" class="form-horizontal" method="POST">
    @csrf
    <div class="container">
        @include('flash::message')
        <div class="row">
            <div class="col-md-8">
                <div class="animated fadeIn">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group required">
                                <label class="col-form-label" for="inputName">Category Name</label>
                                <input class="form-control{{$errors->has('name')?' is-invalid':''}}" name="name" id="inputName"
                                    type="text" placeholder="e.g. Food" value="{{old('name')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="animated fadeIn">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="card-title mb-0">Visibility</strong>
                                </div>
                            </div>
                            <div class="form-group mt-2">
                                <div class="form-check">
                                    <input class="form-check-input" id="radio1" type="radio" value="1" name="active"
                                        {{old('active',1)?"checked":""}}>
                                    <label class="form-check-label" for="radio1">Visible</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="radio2" type="radio" value="0" name="active"
                                        {{old('active',1)?"":"checked"}}>
                                    <label class="form-check-label" for="radio2">Hidden</label>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>
        <hr>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="form-actions float-right">`
                    <button class="btn btn-secondary" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Category</button>
                </div>
            </div>
        </div>
    </div>
</form>
@stop
