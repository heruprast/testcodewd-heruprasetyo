@extends('layouts.master')

@section('content')
<div class="container">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">Manage Category</h4>
                    </div>
                    @if (!auth()->user()->hasRole('cashier'))
                    <div class="col-sm-7 d-none d-md-block">
                        <a class="btn btn-primary float-right" href="{{route('category.create')}}">
                            Add Category
                        </a>
                    </div>
                    @endif
                </div>
                <table class="table table-responsive-sm table-hover mt-4">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                            </th>
                            <th>Category</th>
                            <th>Sort order</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($categories as $item)
                        <tr>
                            <td class="text-center" width="10">
                                <input type="checkbox" name="selected[]" value="{{$item->id}}" />
                            </td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->sort_order}}</td>
                            <td class="text-right">
                                @if (!auth()->user()->hasRole('cashier'))
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('category.edit',$item->id)}}">Edit</a>
                                        <a class="dropdown-item" href="{{route('category.destroy',$item->id)}}"
                                            data-toggle="confirm" data-message="Are you sure?">Hapus</a>
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="table-empty" colspan="4">
                                <div class="table-empty-msg">
                                    <i class="fa fa-warning table-empty-icon"></i> No
                                    records
                                    found
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
