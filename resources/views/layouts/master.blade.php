<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin | {{setting('store_name')}}</title>
    @include('components.stylesheet')
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show {{auth()->user()->hasRole('waiter')?'brand-minimized sidebar-minimized':''}}">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{route('home')}}">
            <img class="navbar-brand-full" src="/admins/img/brand/logo.svg" height="24" alt="CoreUI Logo">
            <img class="navbar-brand-minimized" src="/admins/img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav ml-auto mr-4">
            <li class="nav-item d-md-down-none mr-4">
                <a class="nav-link" href="{{route('pos.index')}}">
                    <i class="icon-grid"></i> POS
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="icofont-user-alt-2"></i>
                    {{auth()->user()->name}}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{route('user.edit',auth()->user()->id)}}">
                        <i class="fa fa-user"></i> Profile</a>
                    <a class="dropdown-item" href="{{route('setting.index')}}">
                        <i class="fa fa-wrench"></i> Settings</a>
                    <a class="dropdown-item" href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fa fa-lock"></i> Logout</a>

                    <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </header>
    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('home')}}">
                            <i class="nav-icon fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    @if (!auth()->user()->hasRole('waiter'))
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="nav-icon cui-lightbulb"></i> Products</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('product.index')}}">
                                    <i class="nav-icon"></i> List Products</a>
                            </li>
                            @if (!auth()->user()->hasRole('cashier'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('product.create')}}">
                                    <i class="nav-icon"></i> Add Product</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="nav-icon icon-folder"></i> Categories</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('category.index')}}">
                                    <i class="nav-icon"></i> List Category</a>
                            </li>
                            @if (!auth()->user()->hasRole('cashier'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('category.create')}}">
                                    <i class="nav-icon"></i> Add Category</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="nav-icon fa fa-th"></i> Tables</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('table.index')}}">
                                    <i class="nav-icon"></i> List Table</a>
                            </li>
                            @if (!auth()->user()->hasRole('cashier'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('table.create')}}">
                                    <i class="nav-icon"></i> Add Table</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="nav-icon  cui-credit-card"></i> Sales</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('sale.index')}}">
                                    <i class="nav-icon"></i> List Sales</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('sale.opened')}}">
                                    <i class="nav-icon"></i> Opened Bill</a>
                            </li>
                        </ul>
                    </li>
                    @if (!auth()->user()->hasRole(['waiter','cashier']))
                    <li class="nav-title">CONFIGURE</li>
                    <li class="divider"></li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="nav-icon  icon-user"></i> User</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('user.index')}}">
                                    <i class="nav-icon"></i> User List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('user.activity')}}">
                                    <i class="nav-icon"></i> User Activity</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('setting.index')}}">
                            <i class="nav-icon cui-cog"></i> Settings</a>
                    </li>
                    @endif

                </ul>
            </nav>
            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
        </div>
        <main class="main">
            {{ Breadcrumbs::render(\Request::route()->getName()) }}
            @yield('content')
        </main>
    </div>
    <footer class="app-footer">
        <div class="ml-auto">
            <span>&copy; 2018 <a href="https://herupras.site">HeruPras.</a></span>
        </div>
    </footer>
    @include('components.scripts')

</body>

</html>
