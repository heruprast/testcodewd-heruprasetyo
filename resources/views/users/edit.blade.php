@extends('layouts.master')

@section('content')
<form action="{{route('user.update',$user->id)}}" class="form-horizontal" method="POST">
    @csrf
    <input name="_method" type="hidden" value="PUT">
    <div class="container">
        @include('flash::message')
        <div class="row">
            <div class="col-md-8">
                <div class="animated fadeIn">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-sm-12">
                                    <strong class="card-title mb-0">User Detail</strong>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="inputName">Full Name</label>
                                <input class="form-control{{$errors->has('name')?' is-invalid':''}}" name="name" id="inputName"
                                    type="text" placeholder="e.g. Heru Prasetyo" value="{{old('name',$user->name)}}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="inputEmail">Email</label>
                                <input class="form-control{{$errors->has('email')?' is-invalid':''}}" name="email" id="inputEmail"
                                    type="text" placeholder="e.g. example@mail.com" value="{{old('email',$user->email)}}"
                                    readonly>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label" for="inputPassword">Password</label>
                                <div class="input-group">
                                    <input type="password" id="input-password" name="password" class="form-control{{$errors->has('password')?' is-invalid':''}}">
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-default" data-toggle="password-generate">Generate
                                            Password</button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" data-toggle="pass-show"> Tampilkan Password
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="animated fadeIn">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="card-title mb-0">Accessibility</strong>
                                </div>
                            </div>
                            <div class="form-group mt-2">
                                @foreach ($roles as $role)
                                <div class="form-check">
                                    <input name="role" class="form-check-input" id="radio-{{$role->name}}" type="radio"
                                        value="{{$role->name}}" name="active"
                                        {{old('role',$user->roles()->first()->name)==$role->name?"checked":""}}>
                                    <label class="form-check-label" for="radio-{{$role->name}}">{{$role->display_name}}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <hr>
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="form-actions float-right">`
                    <button class="btn btn-secondary" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save User</button>
                </div>
            </div>
        </div>
    </div>
</form>
@stop
