@extends('layouts.master')

@section('content')
<div class="container">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">User activity</h4>
                    </div>
                </div>
                <table class="table table-responsive-sm table-hover mt-4 table-sm">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                            <th>User</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($activities as $item)
                        <tr>
                            <td>{{$item->created_at->format('d/m/Y')}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->causer->name}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td class="table-empty" colspan="3">
                                <div class="table-empty-msg">
                                    <i class="fa fa-warning table-empty-icon"></i> No
                                    records
                                    found
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                @if ($activities->hasPages())
                {!! $activities->appends(\Request::except('page'))->render() !!}
                @endif
            </div>
        </div>
    </div>
</div>
@stop
