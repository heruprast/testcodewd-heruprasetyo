@extends('layouts.master')

@section('content')
<div class="container">
    <div class="animated fadeIn">
        @include('flash::message')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">Manage user</h4>
                    </div>
                    <div class="col-sm-7 d-none d-md-block">
                        <a class="btn btn-primary float-right" href="{{route('user.create')}}">
                            Add user
                        </a>
                    </div>
                </div>
                <table class="table table-responsive-sm table-hover mt-4">
                    <thead>
                        <tr>
                            <th class="text-center" width="10">
                                <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                            </th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $user)
                        <tr>
                            <td class="text-center">
                                <input type="checkbox" name="selected[]" value="{{$user->id}}" />
                            </td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->roles()->first()->display_name}}</td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Actions</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('user.edit',$user->id)}}">Edit</a>
                                        @if ($user->id != auth()->user()->id)
                                        <a class="dropdown-item" href="{{route('user.destroy',$user->id)}}" data-toggle="confirm"
                                            data-message="Are you sure?">Hapus</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="table-empty" colspan="4">
                                <div class="table-empty-msg">
                                    <i class="fa fa-warning table-empty-icon"></i> No
                                    records
                                    found
                                </div>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                @if ($users->hasPages())
                {!! $users->appends(\Request::except('page'))->render() !!}
                @endif
            </div>
        </div>
    </div>
</div>
@stop
