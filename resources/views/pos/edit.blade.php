@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="h-100 row mb-4" style="min-height: 650px ;background-color: rgba(255,0,0,0.0);">
        <div class="h-100 col-md-4 d-inline-block m-0">
            <div class="card bg-white">
                <div class="card-header">#{{$order->invoice->number}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('pos.process')}}" method="post" id="process">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                @if ($order)
                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                @endif
                                <select class="form-control" name="table" id="input-table" placeholder="Nomor Meja"
                                    readonly>
                                    @foreach ($tables as $item)
                                    <option value="{{$item->id}}" {{$item->id==$order->table_id?'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div id="print" class="fixed-table-container">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden;height: 378px;">
                                    <div id="list-table-div" style="overflow: hidden; height: 499px;">
                                        <table id="posTable" class="table table-sm">
                                            <thead>
                                                <tr class="success">
                                                    <th>Product</th>
                                                    <th class="text-right">Price</th>
                                                    <th class="text-center">Qty</th>
                                                    <th class="text-right">Subtotal</th>
                                                    <th class="text-center"><i class="fa fa-trash-o"></i></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 499px;"></div>
                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="totaldiv">
                                    <table id="totaltbl" class="table table-condensed totals" style="margin-bottom:10px;">
                                        <tbody>
                                            <tr class="info">
                                                <td width="25%">Total Items</td>
                                                <td class="text-right" style="padding-right:10px;">
                                                    <span id="count">0</span>
                                                </td>
                                                <td width="25%">Total</td>
                                                <td class="text-right" colspan="2"><span id="total" class="total">0</span></td>
                                            </tr>
                                            {{-- <tr class="info">
                                                <td width="25%"><a href="#" id="add_discount">Discount</a></td>
                                                <td class="text-right" style="padding-right:10px;"><span id="ds_con">(0.00)
                                                        0.00</span></td>
                                                <td width="25%"><a href="#" id="add_tax">Order Tax</a></td>
                                                <td class="text-right"><span id="ts_con">3.00</span></td>
                                            </tr> --}}
                                            <tr class="success">
                                                <td colspan="2" style="font-weight:bold;">
                                                    Total Payable
                                                </td>
                                                <td class="text-right" colspan="2" style="font-weight:bold;"><span id="total-payable"
                                                        class="total">0</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <div class="row">
                        <div class="col">
                            <button data-style="slide-down" class="btn btn-primary btn-lg btn-block" type="button" id="hold">Save</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-primary btn-lg btn-block" type="button" id="printBill">Print Bill</button>
                        </div>
                        @if (!$user->hasRole('waiter'))
                        <div class="col">
                            <button data-style="slide-down" class="btn btn-primary btn-lg btn-block" type="button" id="payment"
                                data-url="{{route('sale.paymentDetail',$order->id)}}">Pyment</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="h-100 col-md-8 d-inline-block m-0 p-0">
            <div class="row mb-4">
                <div class="col-12 text-center">
                    <div class="btn-group btn-group-lg d-flex" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success w-100 filter" data-filter="*">All</button>
                        @foreach ($categories as $item)
                        <button type="button" class="btn btn-success w-100 filter" data-filter=".cat-{{$item->id}}">{{$item->name}}</button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="isotope-grid">
                @foreach ($products as $item)
                <span class="grid-item cat-{{$item->category->id}} m-1">
                    <form action="{{route('pos.add')}}" method="post" id="product-{{$item->id}}" style="display: none">
                        @csrf
                        <input type="hidden" name="item" value="{{$item->id}}">
                        <input type="hidden" name="quantity" value="1">
                    </form>
                    <button type="button" data-style="slide-down" class="btn btn-outline-light bg-white product-item"
                        data-id="#product-{{$item->id}}" id="btn-{{$item->id}}" style="color: black">
                        <div class="bg-img"><img src="/{{$item->cover}}" alt="{{$item->name}}" style="width: 100px; height: auto;"></div>
                        <div>{{$item->name}}</div>
                    </button>
                </span>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Payment</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-9">
                        <input type="hidden" name="total">
                        <table class="table table-bordered table-sm mb-4">
                            <tbody>
                                <tr>
                                    <td width="25%" style="border-right-color: #FFF !important;">Total Items</td>
                                    <td width="25%" class="text-right"><span id="item_count">0</span></td>
                                    <td width="25%" style="border-right-color: #FFF !important;">Total Payable</td>
                                    <td width="25%" class="text-right"><span id="twt">0</span></td>
                                </tr>
                                <tr>
                                    <td style="border-right-color: #FFF !important;">Total Paying</td>
                                    <td class="text-right"><span id="total_paying">0</span></td>
                                    <td style="border-right-color: #FFF !important;">Balance</td>
                                    <td class="text-right"><span id="balance">0</span></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-12">
                                <form method="POST" action="{{route('sale.payment')}}" id="pay">
                                    @csrf
                                    <input type="hidden" name="order_id">
                                    <div class="row">
                                        <div class="form-group col-sm-8">
                                            <label for="amount">Amount</label>
                                            <input name="amount" id="amount" type="number" class="medium" data-prefix="Rp."
                                                value="0" step="500">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="paying">Paying by</label>
                                            <select class="form-control" id="paying">
                                                <option value="1">Cash</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputNote">Payment Note</label>
                                        <textarea class="form-control" id="inputNote"></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="btn-group-lg btn-group-vertical d-flex" role="group" aria-label="First group">
                            <button type="button" class="btn btn-outline-primary cash" value="5000">5000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="10000">10000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="20000">20000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="50000">50000</button>
                            <button type="button" class="btn btn-outline-primary cash" value="100000">100000</button>
                            <button type="button" class="btn btn-warning clear" style="color: white">Clear</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-payment">Payment</button>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')
<script>
    $(function () {
        var pTable = $('#posTable').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            responsive: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: {
                url: '/pos/orderList/<?=$order->id?>'
            },
            columns: [{
                data: 'name',
                name: 'name',
                orderable: false,
                searchable: false
            }, {
                data: 'price',
                name: 'price',
                orderable: false,
                searchable: false
            }, {
                data: 'quantity',
                name: 'quantity',
                orderable: false,
                searchable: false
            }, {
                data: 'subtotal',
                name: 'subtotal',
                orderable: false,
                searchable: false
            }, {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }],
            drawCallback: function (settings) {
                total()
            }
        });

        pTable.on('click', '.posdel', function (e) {
            var button = $(this);
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.get(button.data('url'), function (data) {
                    $.growl.notice({
                        message: data.msg
                    });
                    pTable.draw();
                });
            });
        });

        pTable.on('change', '[name="quantity"]', function (e) {
            var input = $(this);
            if (!this.value) {
                input.val(1)
            }
            $.post("/pos/update", {
                id: input.data('id'),
                quantity: input.val()
            }).done(function (data) {
                pTable.draw();
            });
        });

        function total() {
            $.get("/pos/total/<?=$order->id?>", function (data) {
                $('#count').text(data.arg.count);
                $('.total').text(data.arg.total);
                $('[name="order_id"]').val(data.arg.order_id);

                if (data.arg.count == 0) {
                    $('#hold').attr("disabled", "disabled");
                    $('#printBill').attr("disabled", "disabled");
                    $('#payment').attr("disabled", "disabled");
                } else {
                    $('#hold').removeAttr("disabled");
                    $('#printBill').removeAttr("disabled");
                    $('#payment').removeAttr("disabled");
                }
            });
        }

        $('#hold').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $("#process");
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize(),
                    beforeSend: function (xhr, type) {
                        l.start();
                    },
                    success: function (res) {
                        if (res.status == true) {
                            $.growl.notice({
                                message: res.msg
                            });
                            pTable.draw();
                        } else {
                            $.growl.error({
                                message: res.msg
                            });
                        }
                        l.stop();
                    },
                    error: function (e) {
                        $.growl.error({
                            message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                        });
                        l.stop();
                    }
                });
            });
        });

        $('.product-item').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $(button.data('id'));
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function (xhr, type) {
                    l.start();
                },
                success: function (res) {
                    if (res.status == true) {
                        $.growl.notice({
                            message: res.msg
                        });
                        pTable.draw();
                    } else {
                        $.growl.error({
                            message: res.msg
                        });
                    }
                    l.stop();
                },
                error: function (e) {
                    $.growl.error({
                        message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                    });
                    l.stop();
                }
            });

        });

        $('#payment').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var modal = $('#paymentModal')
            l.start()
            $.get(button.data('url'), function (arg) {
                var a = modal.find('#amount')
                var t = modal.find('[name="total"]').val(arg.total)
                modal.find('#item_count').text(arg.item)
                modal.find('#twt').text(arg.total)
                modal.find('#total_paying').text(arg.total)
                modal.find('[name="order_id"]').val(arg.order_id)
                a.val(arg.total)

                modal.find('#balance').text(a.val() - t.val())

            }).done(function () {
                modal.modal('show')
            }).fail(function () {
                $.growl.error({
                    message: "Terjadi kesalahan!"
                });

            }).always(function () {
                l.stop();
            });
        });

        $('.cash').on('click', function (e) {
            var amoun = $('#amount');
            var cash = this.value;
            amoun.val(+amoun.val() + +cash)
            var t = $('[name="total"]')
            $('#balance').text(+amoun.val() - +t.val())
        });
        $('.clear').on('click', function (e) {
            var amoun = $('#amount').val(0)
            var t = $('[name="total"]')
            $('#balance').text(0 - t.val())
        });

        $('#btn-payment').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $("#pay");
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize(),
                    beforeSend: function (xhr, type) {
                        l.start();
                    },
                    success: function (res) {
                        if (res.status == true) {
                            $.growl.notice({
                                message: res.msg
                            });
                            window.location.href = "/pos";
                        } else {
                            $.growl.error({
                                message: res.msg
                            });
                        }
                        l.stop();
                    },
                    error: function (e) {
                        $.growl.error({
                            message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                        });
                        l.stop();
                    }
                });
            });
        });

    });

</script>
@endpush
