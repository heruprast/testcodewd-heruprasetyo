@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="h-100 row mb-4" style="min-height: 650px ;background-color: rgba(255,0,0,0.0);">
        <div class="h-100 col-md-4 d-inline-block m-0">
            <div class="card bg-white">
                <div class="card-header">#{{Carbon\Carbon::now()->format('d/m/Y')}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('pos.process')}}" method="post" id="process">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                <input type="hidden" name="order_id">
                                <select class="form-control" name="table" id="input-table" placeholder="Nomor Meja">
                                    @foreach ($tables as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div id="print" class="fixed-table-container">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden;height: 378px;">
                                    <div id="list-table-div" style="overflow: hidden; height: 499px;">
                                        <table id="posTable" class="table table-sm">
                                            <thead>
                                                <tr class="success">
                                                    <th>Product</th>
                                                    <th class="text-right">Price</th>
                                                    <th class="text-center">Qty</th>
                                                    <th class="text-right">Subtotal</th>
                                                    <th class="text-center"><i class="fa fa-trash-o"></i></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 499px;"></div>
                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
                                <div style="clear:both;"></div>
                                <div id="totaldiv">
                                    <table id="totaltbl" class="table table-condensed totals" style="margin-bottom:10px;">
                                        <tbody>
                                            <tr class="info">
                                                <td width="25%">Total Items</td>
                                                <td class="text-right" style="padding-right:10px;">
                                                    <span id="count">0</span>
                                                </td>
                                                <td width="25%">Total</td>
                                                <td class="text-right" colspan="2"><span id="total" class="total">0</span></td>
                                            </tr>
                                            {{-- <tr class="info">
                                                <td width="25%"><a href="#" id="add_discount">Discount</a></td>
                                                <td class="text-right" style="padding-right:10px;"><span id="ds_con">(0.00)
                                                        0.00</span></td>
                                                <td width="25%"><a href="#" id="add_tax">Order Tax</a></td>
                                                <td class="text-right"><span id="ts_con">3.00</span></td>
                                            </tr> --}}
                                            <tr class="success">
                                                <td colspan="2" style="font-weight:bold;">
                                                    Total Payable
                                                </td>
                                                <td class="text-right" colspan="2" style="font-weight:bold;"><span id="total-payable"
                                                        class="total">0</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-white">
                    <div class="row">
                        <div class="col-6">
                            <button data-style="slide-down" class="btn btn-primary btn-lg btn-block" type="button" id="hold">Hold</button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-lg btn-block" type="button" id="printBill">Print Bill</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="h-100 col-md-8 d-inline-block m-0 p-0">
            <div class="row mb-4">
                <div class="col-12 text-center">
                    <div class="btn-group btn-group-lg d-flex" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success w-100 filter" data-filter="*">All</button>
                        @foreach ($categories as $item)
                        <button type="button" class="btn btn-success w-100 filter" data-filter=".cat-{{$item->id}}">{{$item->name}}</button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="isotope-grid">
                @foreach ($products as $item)
                <span class="grid-item cat-{{$item->category->id}} m-1">
                    <form action="{{route('pos.add')}}" method="post" id="product-{{$item->id}}" style="display: none">
                        @csrf
                        <input type="hidden" name="item" value="{{$item->id}}">
                        <input type="hidden" name="quantity" value="1">
                    </form>
                    <button type="button" data-style="slide-down" class="btn btn-outline-light bg-white product-item"
                        data-id="#product-{{$item->id}}" id="btn-{{$item->id}}" style="color: black">
                        <div class="bg-img"><img src="/{{$item->cover}}" alt="{{$item->name}}" style="width: 100px; height: auto;"></div>
                        <div>{{$item->name}}</div>
                    </button>
                </span>
                @endforeach
            </div>
        </div>
    </div>
</div>

@stop
@push('scripts')
<script>
    $(function () {

        var filter = $('.filter'),
            productItem = $('#isotope-grid');
        if (filter != 'undefined') {
            productItem.isotope({
                itemSelector: '.grid-item',
                layoutMode: 'sloppyMasonry'
            });
            filter.on('click', function () {
                filter.removeClass('active');
                $(this).addClass('active');
                var selector = $(this).attr('data-filter');
                productItem.isotope({
                    filter: selector
                });
                return false;

            });
        }

        var pTable = $('#posTable').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            responsive: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: {
                url: '/pos/orderList'
            },
            columns: [{
                data: 'name',
                name: 'name',
                orderable: false,
                searchable: false
            }, {
                data: 'price',
                name: 'price',
                orderable: false,
                searchable: false
            }, {
                data: 'quantity',
                name: 'quantity',
                orderable: false,
                searchable: false
            }, {
                data: 'subtotal',
                name: 'subtotal',
                orderable: false,
                searchable: false
            }, {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }],
            drawCallback: function (settings) {
                total()
            }
        });

        pTable.on('click', '.posdel', function (e) {
            var button = $(this);
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.get(button.data('url'), function (data) {
                    $.growl.notice({
                        message: data.msg
                    });
                    pTable.draw();
                });
            });
        });

        pTable.on('change', '[name="quantity"]', function (e) {
            var input = $(this);
            if (!this.value) {
                input.val(1)
            }
            $.post("/pos/update", {
                id: input.data('id'),
                quantity: input.val()
            }).done(function (data) {
                pTable.draw();
            });
        });

        function total() {
            $.get("/pos/total", function (data) {
                $('#count').text(data.arg.count);
                $('.total').text(data.arg.total);
                $('[name="order_id"]').val(data.arg.order_id);

                if (data.arg.count == 0) {
                    $('#hold').attr("disabled", "disabled");
                    $('#printBill').attr("disabled", "disabled");
                    $('#payment').attr("disabled", "disabled");
                } else {
                    $('#hold').removeAttr("disabled");
                    $('#printBill').removeAttr("disabled");
                    $('#payment').removeAttr("disabled");
                }
            });
        }

        $('#hold').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $("#process");
            $.alertable.confirm("Apakah anda yakin?").then(function () {
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize(),
                    beforeSend: function (xhr, type) {
                        l.start();
                    },
                    success: function (res) {
                        if (res.status == true) {
                            $.growl.notice({
                                message: res.msg
                            });
                            pTable.draw();
                        } else {
                            $.growl.error({
                                message: res.msg
                            });
                        }
                        l.stop();
                    },
                    error: function (e) {
                        $.growl.error({
                            message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                        });
                        l.stop();
                    }
                });
            });
        });

        $('.product-item').on('click', function (e) {
            var button = $(this);
            var l = Ladda.create(document.querySelector('#' + button.attr('id')));
            var form = $(button.data('id'));
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function (xhr, type) {
                    l.start();
                },
                success: function (res) {
                    if (res.status == true) {
                        $.growl.notice({
                            message: res.msg
                        });
                        pTable.draw();
                    } else {
                        $.growl.error({
                            message: res.msg
                        });
                    }
                    l.stop();
                },
                error: function (e) {
                    $.growl.error({
                        message: "Terjadi kesalahan, Cobalah beberapa saat lagi!"
                    });
                    l.stop();
                }
            });

        });

    });

</script>
@endpush
