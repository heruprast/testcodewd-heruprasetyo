<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);
Route::get('home', ['as' => 'home', 'uses' => 'DashboardController@index']);

Route::group(['prefix' => 'categories', 'as' => 'category.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'CategoryController@index']);
    Route::post('/', ['as' => 'store', 'uses' => 'CategoryController@store']);
    Route::get('new', ['as' => 'create', 'uses' => 'CategoryController@create']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'CategoryController@edit']);
    Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'CategoryController@destroy']);
    Route::put('update/{id}', ['as' => 'update', 'uses' => 'CategoryController@update']);
});

Route::group(['prefix' => 'tables', 'as' => 'table.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'TableController@index']);
    Route::post('/', ['as' => 'store', 'uses' => 'TableController@store']);
    Route::get('new', ['as' => 'create', 'uses' => 'TableController@create']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'TableController@edit']);
    Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'TableController@destroy']);
    Route::put('update/{id}', ['as' => 'update', 'uses' => 'TableController@update']);
});

Route::group(['prefix' => 'pos', 'as' => 'pos.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PosController@index']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'PosController@edit']);
    Route::post('/', ['as' => 'add', 'uses' => 'PosController@add']);
    Route::get('orderList', 'PosController@orderList');
    Route::get('orderList/{id}', 'PosController@orderListEdit');
    Route::get('total', 'PosController@total');
    Route::get('total/{id}', 'PosController@totalEdit');
    Route::post('update', 'PosController@update');
    Route::put('process', ['as' => 'process', 'uses' => 'PosController@process']);
    Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'PosController@destroy']);
    Route::post('payment', ['as' => 'payment', 'uses' => 'PosController@payment']);
    Route::get('payment/{id}', ['as' => 'paymentDetail', 'uses' => 'PosController@paymentDetail']);
});

Route::group(['prefix' => 'products', 'as' => 'product.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
    Route::post('/', ['as' => 'store', 'uses' => 'ProductController@store']);
    Route::get('inventory', ['as' => 'inventory', 'uses' => 'ProductController@inventory']);
    Route::get('new', ['as' => 'create', 'uses' => 'ProductController@create']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'ProductController@edit']);
    Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'ProductController@destroy']);
    Route::put('update/{id}', ['as' => 'update', 'uses' => 'ProductController@update']);

});

Route::group(['prefix' => 'sales', 'as' => 'sale.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'SaleController@index']);
    Route::get('opened', ['as' => 'opened', 'uses' => 'SaleController@opened']);
    Route::get('show/{id}', ['as' => 'show', 'uses' => 'SaleController@show']);
    Route::post('payment', ['as' => 'payment', 'uses' => 'SaleController@payment']);
    Route::get('payment/{id}', ['as' => 'paymentDetail', 'uses' => 'SaleController@paymentDetail']);
});

Route::group(['prefix' => 'users', 'as' => 'user.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'UserController@index']);
    Route::get('activity', ['as' => 'activity', 'uses' => 'UserController@activity']);
    Route::post('/', ['as' => 'store', 'uses' => 'UserController@store']);
    Route::get('new', ['as' => 'create', 'uses' => 'UserController@create']);
    Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'UserController@edit']);
    Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'UserController@destroy']);
    Route::put('update/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
});

Route::group(['prefix' => 'settings', 'as' => 'setting.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'SettingController@index']);
    Route::put('/', ['as' => 'update', 'uses' => 'SettingController@update']);
});

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@loginForm']);
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
});
