<?php

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('sale.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Sale', route('sale.index'));
});

Breadcrumbs::for('sale.opened', function ($trail) {
    $trail->parent('sale.index');
    $trail->push('Opened');
});

Breadcrumbs::for('pos.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Pos', route('pos.index'));
});

Breadcrumbs::for('pos.edit', function ($trail) {
    $trail->parent('pos.index');
    $trail->push('Edit');
});

Breadcrumbs::for('product.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Product', route('product.index'));
});

Breadcrumbs::for('product.create', function ($trail) {
    $trail->parent('product.index');
    $trail->push('New Product', route('product.create'));
});

Breadcrumbs::for('product.edit', function ($trail) {
    $trail->parent('product.index');
    $trail->push('Edit Product');
});

Breadcrumbs::for('category.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Category', route('category.index'));
});

Breadcrumbs::for('category.create', function ($trail) {
    $trail->parent('category.index');
    $trail->push('New Category', route('category.create'));
});

Breadcrumbs::for('category.edit', function ($trail) {
    $trail->parent('category.index');
    $trail->push('Edit Category');
});

Breadcrumbs::for('table.index', function ($trail) {
    $trail->parent('home');
    $trail->push('table', route('table.index'));
});

Breadcrumbs::for('table.create', function ($trail) {
    $trail->parent('table.index');
    $trail->push('New table', route('table.create'));
});

Breadcrumbs::for('table.edit', function ($trail) {
    $trail->parent('table.index');
    $trail->push('Edit table');
});

Breadcrumbs::for('user.index', function ($trail) {
    $trail->parent('home');
    $trail->push('User', route('user.index'));
});

Breadcrumbs::for('user.create', function ($trail) {
    $trail->parent('user.index');
    $trail->push('New User', route('user.create'));
});

Breadcrumbs::for('user.activity', function ($trail) {
    $trail->parent('user.index');
    $trail->push('Activity');
});

Breadcrumbs::for('user.edit', function ($trail) {
    $trail->parent('user.index');
    $trail->push('Edit User');
});

Breadcrumbs::for('setting.index', function ($trail) {
    $trail->parent('home');
    $trail->push('setting', route('setting.index'));
});
