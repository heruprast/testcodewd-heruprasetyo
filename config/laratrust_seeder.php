<?php

return [
    'role_structure'  => [
        'admin'   => [
            'users'   => 'c,r,u,d',
            'profile' => 'r,u',
        ],
        'cashier' => [
            'profile' => 'r,u',
        ],
        'waiter'  => [
            'profile' => 'r,u',
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
    ],
];
