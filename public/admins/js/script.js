$(function () {

    let token = document.head.querySelector('meta[name="csrf-token"]');
    if (!token) {
        $.alertable.alert('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    $('[data-toggle="confirm"]').on('click', function (e) {
        e.preventDefault();
        var link = $(this);
        $.alertable.confirm(link.data('message')).then(function () {
            window.location.href = link.attr('href');
        });
    });

    $('.editor').summernote({
        height: 250
    });

    $('.tag').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        create: function (input) {
            return {
                value: input,
                text: input
            }
        }
    });

    var $inputNumber = $("input[type='number'].medium")
    $inputNumber.inputSpinner()

    $inputNumber.on("change", function (event) {
        if (!this.value) {
            $(this).val(0)
        }
    })

    var $inputNumberS = $("input[type='number'].small")
    $inputNumberS.inputSpinner({
        groupClass: "input-group-sm"
    })

    $inputNumberS.on("change", function (event) {
        if (!this.value) {
            $(this).val(0)
        }
    })
    new InputFile();
})
