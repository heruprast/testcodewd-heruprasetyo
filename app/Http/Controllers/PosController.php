<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderInvoice;
use App\Models\OrderList;
use App\Models\OrderPayment;
use App\Models\Product;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\Datatables;

class PosController extends Controller
{

    public function index()
    {

        $user = Auth::user();
        $data = [
            'categories' => Category::all(),
            'products'   => Product::all(),
            'tables'     => Table::all(),
            'order'      => $user->orders->where('status', 0)->first(),
            'user'       => $user,
        ];
        return view('pos.index', $data);
    }

    public function edit($id)
    {
        $user  = Auth::user();
        $order = Order::find($id);
        $data  = [
            'categories' => Category::all(),
            'products'   => Product::all(),
            'tables'     => Table::all(),
            'order'      => $order,
            'user'       => $user,
        ];
        return view('pos.edit', $data);
    }

    public function orderList()
    {
        $user       = Auth::user();
        $order      = $user->orders->where('status', 0)->first();
        $orderId    = ($order) ? $order->id : 0;
        $orderLists = OrderList::where('order_id', $orderId)
            ->orderBy('created_at', 'ASC')
            ->get();
        return Datatables::of($orderLists)
            ->addColumn('name', function ($item) {
                return '<strong>' . $item->product->name . '</strong>';
            })
            ->addColumn('price', function ($item) {
                return '<span class="text-right">' . $item->product->price . '</span>';
            })
            ->addColumn('quantity', function ($item) {
                return '<input class="form-control form-control-sm text-center" name="quantity"  type="number" min="1" value="' . $item->quantity . '" data-id="' . $item->id . '">';
            })
            ->addColumn('subtotal', function ($item) {
                return '<span class="text-right">' . $item->subtotal . '</span>';
            })
            ->addColumn('action', function ($item) {
                return '<i class="fa fa-trash-o tip pointer posdel" title="Remove" data-url="' . route('pos.delete', $item->id) . '"></i>';
            })->rawColumns(['name', 'price', 'quantity', 'subtotal', 'action'])->make(true);
    }

    public function orderListEdit($id)
    {
        $user       = Auth::user();
        $order      = Order::find($id);
        $orderLists = OrderList::where('order_id', $order->id)
            ->orderBy('created_at', 'ASC')
            ->get();
        return Datatables::of($orderLists)
            ->addColumn('name', function ($item) {
                return '<strong>' . $item->product->name . '</strong>';
            })
            ->addColumn('price', function ($item) {
                return '<span class="text-right">' . $item->product->price . '</span>';
            })
            ->addColumn('quantity', function ($item) {
                return '<input class="form-control form-control-sm text-center" name="quantity"  type="number" min="1" value="' . $item->quantity . '" data-id="' . $item->id . '">';
            })
            ->addColumn('subtotal', function ($item) {
                return '<span class="text-right">' . $item->subtotal . '</span>';
            })
            ->addColumn('action', function ($item) {
                return '<i class="fa fa-trash-o tip pointer posdel" title="Remove" data-url="' . route('pos.delete', $item->id) . '"></i>';
            })->rawColumns(['name', 'price', 'quantity', 'subtotal', 'action'])->make(true);
    }

    public function process(Request $request)
    {
        $invoiceNumber = OrderInvoice::getNumber();

        $order           = Order::find($request->order_id);
        $order->table_id = $request->table;
        $order->status   = 2;
        $order->save();

        $invoice = new OrderInvoice([
            'number' => $invoiceNumber,
        ]);
        $invoice->order()->associate($order);
        $invoice->save();

        $payment = new OrderPayment([
            'payable'   => $order->total,
            'paying'    => 0,
            'balance'   => 0,
            'paying_by' => 0,
            'note'      => $request->note,
        ]);
        $payment->order()->associate($order);
        $payment->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menambahkan pesanan baru.');

        return response()->json(['msg' => 'Order has been saved.', 'status' => true], 200);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric',
            'item'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'Something wrong!', 'status' => false], 200);
        }
        $product = Product::find($request->item);
        $user    = Auth::user();
        $table   = Table::find(1);

        $order = $user->orders->where('status', 0)->first();

        if (is_null($order)) {
            $order = new Order([
                'table_id' => 0,
            ]);
            $order->user()->associate($user);
            $order->save();
        }

        if (!$this->hasList($order->id, $product->id)) {
            $orderList = new OrderList([
                'quantity' => 1,
            ]);
            $orderList->product()->associate($product);
            $orderList->order()->associate($order);
            $orderList->save();
        } else {
            $orderList = OrderList::where('order_id', $order->id)
                ->where('product_id', $product->id)
                ->first()
                ->increment('quantity', 1);
        }

        return response()->json(['msg' => 'Has been saved!', 'status' => true], 200);

    }

    private function hasList($orderId, $productId)
    {
        $order = Order::find($orderId);
        return !is_null($order->lists->where('product_id', $productId)->first());
    }

    public function total()
    {
        $user  = Auth::user();
        $order = $user->orders->where('status', 0)->first();

        $data = [
            'order_id' => $order ? $order->id : 0,
            'total'    => $order ? $order->total : 0,
            'count'    => $order ? count($order->lists) : 0,
        ];
        return response()->json(['arg' => $data, 'status' => true], 200);
    }
    public function totalEdit($id)
    {
        $order = Order::find($id);

        $data = [
            'order_id' => $order ? $order->id : 0,
            'total'    => $order ? $order->total : 0,
            'count'    => $order ? count($order->lists) : 0,
        ];
        return response()->json(['arg' => $data, 'status' => true], 200);
    }

    public function update(Request $request)
    {
        $order           = OrderList::find($request->id);
        $order->quantity = $request->quantity;
        $order->save();
    }

    public function payment(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'Something wrong!', 'status' => false], 200);
        }

        $order = Order::find($request->order_id);
        if ($request->amount < $order->total) {
            return response()->json(['msg' => 'Smaller amount than payable!', 'status' => false], 200);
        }
        $order->status = 1;
        $order->save();

        $payment = new OrderPayment([
            'payable'   => $order->total,
            'paying'    => $request->amount,
            'balance'   => $request->amount - $order->total,
            'paying_by' => 1,
            'note'      => $request->note,
        ]);
        $payment->order()->associate($order);
        $payment->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menyelaisaikan pesanan.');

        return response()->json(['msg' => 'Order has been saved.', 'status' => true], 200);
    }

    public function destroy($id)
    {
        $order = OrderList::find($id);
        $order->delete();
        return response()->json(['msg' => 'Order has been deleted.', 'status' => true], 200);
    }

}
