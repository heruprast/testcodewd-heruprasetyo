<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TableController extends Controller
{

    public function index()
    {
        $tables = Table::paginate(20);
        return view('table.index', compact('tables'));
    }

    public function create()
    {
        return view('table.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }

        $table = new Table([
            'name'   => $request->name,
            'active' => $request->active,
        ]);
        $table->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menambahkan kategori baru.');

        flash('Success: You have modified tables!')->success();
        return redirect()->route('table.index');
    }

    public function edit($id)
    {
        $table = Table::find($id);
        return view('table.edit', \compact('table'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }
        $table         = Table::find($id);
        $table->name   = $request->name;
        $table->active = $request->active;
        $table->update();

        flash('Success: You have modified tables!')->success();
        return back();
    }

    public function destroy($id)
    {
        $table = Table::find($id);
        $table->delete();

        flash('Success: You have deleted tables!')->success();
        return back();
    }
}
