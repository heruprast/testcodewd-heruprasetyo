<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Login failed', [], 200);
        }

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = auth()->user();

            activity()->causedBy($user)->log($user->name . ' barusaja login.');
            return $this->sendResponse($this->userData($user), 'User login successfully.');
        }

        return $this->sendError(trans('auth.failed'), [], 200);
    }

    public function userData($user)
    {
        $result = [
            'id'    => $user->id,
            'name'  => $user->name,
            'email' => $user->email,
            'role'  => $user->roles()->first()->name,
        ];
        return $result;
    }
}
