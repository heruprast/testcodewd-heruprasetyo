<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ApiController extends Controller
{
    public function sendResponse($result, $message)
    {
        $response = [
            'status'  => true,
            'values'  => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'status'  => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['values'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
