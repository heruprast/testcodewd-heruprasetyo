<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends ApiController
{
    public function data(Request $request)
    {
        $products = [];
        $results  = Product::orderBy('created_at', 'DESC')->paginate(18);

        foreach ($results as $product) {
            $row              = [];
            $row['id']        = $product->id;
            $row['name']      = $product->name;
            $row['price_old'] = $product->price;
            $row['cover']     = $product->cover;
            $products[]       = $row;
        }
        $data = [
            'current_page'   => $results->currentPage(),
            'products'       => $products,
            'first_page_url' => $results->url(1),
            'from'           => $results->firstItem(),
            'last_page'      => $results->lastPage(),
            'last_page_url'  => $results->url($results->lastPage()),
            'next_page_url'  => $results->nextPageUrl(),
            'per_page'       => $results->perPage(),
            'prev_page_url'  => $results->previousPageUrl(),
            'to'             => $results->count(),
            'total'          => $results->total(),
            'has_more_page'  => $results->hasMorePages(),
        ];
        return $this->sendResponse($data, 'Posts retrieved successfully.');
    }
}
