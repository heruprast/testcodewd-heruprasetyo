<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Image;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::paginate(20);
        return view('product.index', compact('products'));
    }

    public function inventory()
    {
        return view('product.inventory');
    }

    public function create()
    {
        $categories = Category::all();
        return view('product.create', \compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required',
            'image' => 'nullable|mimes:jpeg,jpg,png',
        ]);
        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }

        $user     = Auth::user();
        $category = Category::find($request->category);

        $product         = new Product;
        $product->name   = $request->title;
        $product->price  = $request->price;
        $product->status = 1;
        if ($request->hasFile('image')) {
            $image     = $request->file('image');
            $extension = $image->clientExtension();
            $fileName  = str_slug($request->title) . time() . ".{$extension}";
            $path      = public_path('images/products/' . $fileName);
            Image::make($image->getRealPath())->crop(200, 200)->save($path);

            $product->image = "images/products/{$fileName}";
        }

        $product->category()->associate($category);
        $product->user()->associate($user);
        $product->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menambahkan produk baru.');

        flash('Success: Product has been saved!')->success();
        return redirect()->route('product.index');

    }
    public function edit($id)
    {
        $categories = Category::all();
        $product    = Product::find($id);
        return view('product.edit', compact('product', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required',
            'image' => 'nullable|mimes:jpeg,jpg,png',
        ]);
        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }

        $category = Category::find($request->category);

        $product         = Product::find($id);
        $product->name   = $request->title;
        $product->price  = $request->price;
        $product->status = 1;

        if ($request->hasFile('image')) {
            if (file_exists(public_path($product->image)) && $product->image) {
                File::delete($product->image);
            }

            $image     = $request->file('image');
            $extension = $image->clientExtension();
            $fileName  = str_slug($request->title) . time() . ".{$extension}";
            $path      = public_path('images/products/' . $fileName);
            Image::make($image->getRealPath())->crop(200, 200)->save($path);

            $product->image = "images/products/{$fileName}";
        }

        $product->category()->associate($category);
        $product->save();

        flash('Success:Y ou have modified product!')->success();
        return redirect()->route('product.edit', $id);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        flash('Success: You have deleted product!')->success();
        return back();
    }

}
