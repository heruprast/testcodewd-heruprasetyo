<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::paginate(20);
        return view('category.index', compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }

        $category = new Category([
            'name'   => $request->name,
            'active' => $request->active,
        ]);
        $category->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menambahkan kategori baru.');

        flash('Success: You have modified categories!')->success();
        return redirect()->route('category.index');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit', \compact('category'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }
        $category         = Category::find($id);
        $category->name   = $request->name;
        $category->active = $request->active;
        $category->update();

        flash('Success: You have modified categories!')->success();
        return back();
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        flash('Success: You have deleted categories!')->success();
        return back();
    }
}
