<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{

    public function index()
    {
        $orders = new Order;
        $user   = Auth::user();

        if ($user->hasRole('waiter')) {
            $orders = $orders->where('user_id', $user->id);
        }
        $orders = $orders->where('status', 1)->paginate(20);
        return view('sales.index', \compact('orders', 'user'));
    }

    public function opened()
    {
        $orders = new Order;
        $user   = Auth::user();
        if ($user->hasRole('waiter')) {
            $orders = $orders->where('user_id', $user->id);
        }
        $orders = $orders->where('status', 2)->paginate(20);
        return view('sales.index', \compact('orders', 'user'));
    }

    public function show(Request $request, $id)
    {

        if ($request->ajax()) {
            $order = Order::find($id);

            $items = [];
            foreach ($order->lists as $item) {
                $row             = [];
                $row['name']     = $item->product->name;
                $row['quantity'] = $item->quantity;
                $row['price']    = $item->product->price;
                $row['subtotal'] = $item->subtotal;
                $items[]         = $row;
            }

            $result = [
                'total'      => $order->total,
                'grandTotal' => $order->total,
                'date'       => $order->created_at->format('d M, Y, h:m a'),
                'ref'        => $order->invoice->number,
                'items'      => $items,
                'waiter'     => $order->user->name,
                'paidBy'     => "Cash",
                'amount'     => $order->payment->paying,
                'change'     => $order->payment->balance,
                'cashier'    => $order->user->name,
            ];
            return response()->json($result, 200);
        }
        return abort(404);
    }

    public function paymentDetail(Request $request, $id)
    {
        if ($request->ajax()) {
            $order  = Order::find($id);
            $result = [
                'order_id' => $order->id,
                'total'    => $order->total,
                'item'     => count($order->lists),
            ];
            return response()->json($result, 200);
        }
        return abort(404);
    }
    public function payment(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'Something wrong!', 'status' => false], 200);
        }

        $order = Order::find($request->order_id);
        if ($request->amount < $order->total) {
            return response()->json(['msg' => 'Smaller amount than payable!', 'status' => false], 200);
        }

        $order->status = 1;
        $order->save();

        $invoice         = $order->invoice;
        $invoice->status = 1;
        $invoice->save();

        $payment            = $order->payment;
        $payment->payable   = $order->total;
        $payment->paying    = $request->amount;
        $payment->balance   = $request->amount - $order->total;
        $payment->paying_by = 1;
        $payment->note      = $request->note;
        $payment->save();

        $user = Auth::user();
        activity()->causedBy($user)->log($user->name . ' menyelaisaikan pesanan.');

        return response()->json(['msg' => 'Order has been saved.', 'status' => true], 200);
    }

}
