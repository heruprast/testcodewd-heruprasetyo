<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Setting;

class SettingController extends Controller
{
    public function index()
    {
        return view('setting.index');
    }

    public function update(Request $request)
    {

        Setting::set('store_name', $request->title);
        Setting::set('store_address', $request->address);

        flash('Success: You have modified setting!')->success();
        return back();
    }
}
