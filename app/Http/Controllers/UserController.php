<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;

class UserController extends Controller
{

    public function index()
    {

        $users = User::paginate(20);
        return view('users.index', \compact('users'));
    }

    public function activity()
    {

        $activities = Activity::orderBy('created_at', 'DESC')->paginate(100);
        return view('users.activity', \compact('activities'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('users.create', \compact('roles'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'required|unique:users,email',
            'password' => 'required|string|min:6',
            'role'     => 'required',
        ]);
        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }
        $user                    = new User();
        $user->name              = $request->name;
        $user->email             = $request->email;
        $user->email_verified_at = Carbon::now();
        $user->password          = bcrypt($request->password);
        $user->save();
        $user->attachRole($request->role);

        flash('Success: You have saved user!')->success();
        return back();
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user  = User::find($id);
        return view('users.edit', \compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'password' => 'nullable|string|min:6',
            'role'     => 'required',
        ]);
        if ($validator->fails()) {
            flash('Warning: Please check the form carefully for errors!')->error();
            return back()->withInput()->withErrors($validator);
        }
        $user       = User::find($id);
        $user->name = $request->name;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        $user->syncRoles([$request->role]);

        flash('Success: You have modified user!')->success();
        return back();
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        flash('Success: User deleted successfully.')->success();
        return back();
    }
}
