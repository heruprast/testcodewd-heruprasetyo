<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            $result = ['msg' => 'Email belum terdaftar!', 'status' => false];
        } else {
            $remember = $request->get('remember');
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
                $result = ['callback' => route('home'), 'status' => true];
                $user   = Auth::user();
                activity()->causedBy($user)->log($user->name . ' barusaja login.');
            } else {
                $result = ['msg' => 'Password tidak benar, silakan ulangi!', 'status' => false];
            }
        }

        if ($request->ajax()) {
            return response()->json($result, 200);
        }
        return back()->withInput()->withErrors($validator);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('auth.login');
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
