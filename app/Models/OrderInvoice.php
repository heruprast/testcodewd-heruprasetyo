<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInvoice extends Model
{
    protected $fillable = [
        'order_id', 'number', 'status',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function scopeGetNumber()
    {
        $date       = \Carbon\Carbon::now();
        $prefix     = sprintf("%s%s-", "ERP", $date->format('dmY'));
        $lastNumber = self::orderBy('number', 'desc')->first();
        if (!is_null($lastNumber)) {
            $lastNumberNo = $lastNumber->number;
            if (substr($lastNumberNo, -12, 8) == $date->format('dmY')) {
                return ++$lastNumberNo;
            }

        }
        return $prefix . '001';
    }
}
