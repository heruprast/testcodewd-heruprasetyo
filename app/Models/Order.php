<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'table_id', 'user_id', 'status',
    ];

    public function lists()
    {
        return $this->hasMany(OrderList::class);
    }

    public function invoice()
    {
        return $this->hasOne(OrderInvoice::class);
    }

    public function payment()
    {
        return $this->hasOne(OrderPayment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function table()
    {
        return $this->belongsTo(Table::class)->withTrashed();
    }

    public function getTotalAttribute()
    {
        $cost = 0;
        foreach ($this->lists as $item) {
            $cost = $cost + ($item->quantity * $item->product->price);
        }
        return $cost;
    }
}
