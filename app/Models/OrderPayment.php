<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $fillable = [
        'order_id',
        'payable',
        'paying',
        'balance',
        'paying_by',
        'note',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
