<?php

use App\Models\Table;
use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();
        $this->command->info('Creating Meja 01');
        Table::create([
            'name' => 'Meja 01',
        ]);

        $this->command->info('Creating Meja 02');
        Table::create([
            'name' => 'Meja 02',
        ]);

        $this->command->info('Creating Meja 03');
        Table::create([
            'name' => 'Meja 03',
        ]);
    }

    public function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('tables')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
