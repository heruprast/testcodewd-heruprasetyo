<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();

        $this->command->info('Creating user Admin');
        $user = \App\Models\User::create([
            'name'              => "Administrator",
            'email'             => "admin@demo.com",
            'password'          => bcrypt('demo123'),
            'email_verified_at' => Carbon\Carbon::now(),
        ]);
        $user->attachRole('admin');

        $this->command->info('Creating user Cashier');
        $user = \App\Models\User::create([
            'name'              => "Cashier",
            'email'             => "cashier@demo.com",
            'password'          => bcrypt('demo123'),
            'email_verified_at' => Carbon\Carbon::now(),
        ]);
        $user->attachRole('cashier');

        $this->command->info('Creating user Waiter');
        $user = \App\Models\User::create([
            'name'              => "Waiter",
            'email'             => "waiter@demo.com",
            'password'          => bcrypt('demo123'),
            'email_verified_at' => Carbon\Carbon::now(),
        ]);
        $user->attachRole('waiter');
    }

    public function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        DB::table('role_user')->truncate();
        \App\Models\User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
