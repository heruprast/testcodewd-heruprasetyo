<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();
        $this->command->info('Creating product Indomi Goreng');
        Product::create([
            'user_id'     => 1,
            'category_id' => 1,
            'name'        => 'Indomi Goreng',
            'price'       => 5000,
            'image'       => 'images/products/indomi-goreng1548874717.jpeg',
        ]);

        $this->command->info('Creating product Ayam Goreng');
        Product::create([
            'user_id'     => 1,
            'category_id' => 1,
            'name'        => 'Ayam Goreng',
            'price'       => 25000,
            'image'       => 'images/products/ayam-goreng1548874737.jpeg',
        ]);

        $this->command->info('Creating product Es Teler');
        Product::create([
            'user_id'     => 1,
            'category_id' => 2,
            'name'        => 'Es Teler',
            'price'       => 2500,
            'image'       => 'images/products/es-teler1548890155.jpeg',
        ]);
    }

    public function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('products')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
