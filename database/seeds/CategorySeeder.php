<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();
        $this->command->info('Creating category Makanan');
        Category::create([
            'name' => 'Makanan',
        ]);

        $this->command->info('Creating category Minuman');
        Category::create([
            'name' => 'Minuman',
        ]);
    }

    public function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('categories')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
